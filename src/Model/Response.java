package Model;

public class Response {
    private String filename;
//    private String fileExt;
//    private String fileEncode;
    private String data;
    private String method;

    public Response(String filename, String data, String method) {
        this.filename = filename;
        this.data = data;
        this.method = method;
    }

    public Response() {
    }

    public String getFilename() {
        return filename;
    }

    public String getData() {
        return data;
    }

    public String getMethod() {
        return method;
    }
}
