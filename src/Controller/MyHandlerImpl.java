package Controller;

import Model.Request;
import Model.Response;
import Utility.EncodingDecoding;


public class MyHandlerImpl implements MyHandler {
    EncodingDecoding encodingDecoding = new EncodingDecoding();
    String path;

    @Override
    public void reqHandler(Request request) {
        path = "/home/altaf/Music/" + request.getFilename() + "_uploaded" + "." + request.getFileExt();
        encodingDecoding.decoder(request.getData(), path);
    }

    @Override
    public void resHandler(Response response) {
        String username = System.getProperty("user.name");
        path = "/home/"+username+"/Videos/"+ "_downloaded_" + response.getFilename();
        encodingDecoding.decoder(response.getData(), path);
    }
}
