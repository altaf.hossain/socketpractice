package Controller;

import Model.Request;
import Model.Response;

public interface MyHandler {
        void reqHandler(Request request);
        void resHandler(Response response);

}
