package ServerClient;


import Controller.MyHandlerImpl;
import Model.Request;
import Model.Response;
import Utility.EncodingDecoding;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {


    private ServerSocket serverSocket;

    public void start(int port) {
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Server Connected waiting for Client..");
            while (true)
                new ClientHandler(serverSocket.accept()).start();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            stop();
        }

    }

    public void stop() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static class ClientHandler extends Thread {
        private Socket clientSocket;
        private PrintWriter out;
        private BufferedReader in;


        public ClientHandler(Socket socket) {
            this.clientSocket = socket;
        }

        public void run() {
            parser();
        }


        //method for parsing....
        private void parser() {
            Request request;
            Response response;
            try {
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                String line = "";
                line = in.readLine();
                if (line == null) {
                    System.out.println("Nothing to read");
                    out.print("File uploading failed");
                    out.flush();
                    return;
                }

                while (line != null) {
                    String[] rcvStrings = line.split(",");
                    String fileN = rcvStrings[0];
                    String method = rcvStrings[1];
                    if(method.equals("send")){
                        String fileExt = rcvStrings[2];
                        String fileEncode = rcvStrings[3];
                        String encoder = rcvStrings[4];
                        request = new Request(fileN,fileExt,fileEncode,encoder,method);
                        new MyHandlerImpl().reqHandler(request);
                        out.println("File Uploaded Successfully");
                        out.flush();
                    }else {
                        String data = fetchFile(fileN);
                        if (data.isEmpty()){
                            out.println("File not found");
                        }else {
                            response = new Response(fileN,data,method);
                            new MyHandlerImpl().resHandler(response);
                            out.println("File download Successfully");
                        }
                        out.flush();

                    }

                    break;
                }
//                out.println("File Uploading Successful");
//                out.flush();
                System.out.println("end of transmission");


            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        public String fetchFile(String fileName){
            String username = System.getProperty("user.name");
            String path = "/home/"+username+"/Pictures/" + fileName;
            EncodingDecoding encodingDecoding = new EncodingDecoding();
            String encoder = encodingDecoding.encoder(path);
            return encoder;
        }

    }



    public static void main(String[] args) {
        Server server = new Server();
        server.start(8888);
    }


}
