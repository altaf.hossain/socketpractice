package ServerClient;

import Utility.EncodingDecoding;

import java.io.*;
import java.net.Socket;

public class Client extends Thread{

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    EncodingDecoding encoDeco;

    public Client() {
        encoDeco = new EncodingDecoding();
    }

    public void startConnection(String ip, int port) {
        try {
            clientSocket = new Socket(ip, port);
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException e) {
            System.err.println(e);
        }

    }

    public String sendMessage(String msg) {
        try {
            out.println(msg);
            return in.readLine();
        } catch (Exception e) {
            return null;
        }
    }

    public String takeUserInput(String clue) {
        BufferedReader brCli = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter image name to "+ clue +" Server: ");
        try {
            String fileName = brCli.readLine();

            return fileName;
        } catch (IOException e) {
            return null;
        }

    }


    public String sendFile(String method, String fileExt, String fileEncode) {
        try {
            String clue = "send to";
            String fileName= takeUserInput(clue);
            String username = System.getProperty("user.name");
            String path = "/home/"+username+"/Pictures/" + fileName + "." + fileExt;
            String data = encoDeco.encoder(path);
            out.println(fileName + "," + method + "," + fileExt + "," + fileEncode + "," + data);
            return in.readLine();
        } catch (Exception e) {
           return null;
        }
    }

    public String fetchFile(String method) {
        try {
            String clue = "fetch from";
            String fileName= takeUserInput(clue);
            out.println(fileName + "," + method);
            return in.readLine();
        } catch (Exception e) {
            return null;
        }
    }



    public void stopConnection() {
        try {
            in.close();
            out.close();
            clientSocket.close();
        } catch (IOException e) {
            System.err.println(e);
        }

    }

}
