package Testing;

import ServerClient.Client;

public class TestingClientWithServer {
    public static void main(String[] args) {
        Client client1 = new Client();
        client1.startConnection("127.0.0.1", 8888);
        // for sending any file to server, the image must be in u'r Photos directory. no need to give the path just type the image name without extension.
        // u'r image must be jpeg or you can change the extension in argument if you want.
        // String s = client1.sendFile("send", "jpeg", "base64");

        //for fetching any file from server it will download to in your Videos directory & server will find the image in Photos directory.
        // give the file name with extension for fetch
        String s = client1.fetchFile("fetch");
        System.out.println("server response :: " + s);
    }
}
